package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	file, err := os.ReadFile("./cmd/somefile.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(file))
}
